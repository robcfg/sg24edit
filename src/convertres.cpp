#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

string files[28] = { "SG24IcnG.bmp", "Tile11.bmp", "Tile16.bmp", "Tile20.bmp", \
                   "Tile25.bmp", "Tile7.bmp", "SG24Icon.bmp", "Tile12.bmp",  \
                   "Tile17.bmp", "Tile21.bmp", "Tile3.bmp", "Tile8.bmp",     \
                   "SG24Scrn.bmp", "Tile13.bmp", "Tile18.bmp", "Tile22.bmp",\
                   "Tile4.bmp", "Tile9.bmp", "Tile1.bmp", "Tile14.bmp",      \
                   "Tile19.bmp", "Tile23.bmp", "Tile5.bmp", "Tile10.bmp",    \
                   "Tile15.bmp", "Tile2.bmp", "Tile24.bmp", "Tile6.bmp" };

ifstream::pos_type GetFileSize(const char* filename)
{
    ifstream in(filename, std::ifstream::binary | std::ifstream::ate);
    return in.tellg(); 
}

size_t ProcessFile(const string& filename, FILE* outfile)
{
	string strName = filename.substr(0, filename.length()-4);
	string finalname = "./res/";
	finalname += filename;

	size_t filesize = (size_t)GetFileSize(finalname.c_str());

	FILE* infile = fopen(finalname.c_str(),"rb");

	unsigned char tmp;

	fprintf(outfile,"size_t %s_size = %lu;\n",strName.c_str(),filesize);
	fprintf(outfile,"static const unsigned char %s[%lu] = {",strName.c_str(),filesize);

	for( size_t byte = 0; byte < filesize; ++byte )
	{
		fread(&tmp,1,1,infile);
		fprintf(outfile,"%u",tmp);

		if(byte < filesize - 1)
			fprintf(outfile,",");
	}

	fprintf(outfile,"};\n\n");

	return filesize;
}

int main(int argc, char** argv)
{
	// Open output file
	//ofstream pafuera("resources.h",ios::binary);

	//pafuera << "// resources.h - SG24Edit graphics" << endl << endl;

	FILE* pafuera = fopen("resources.h","w");
	fprintf(pafuera,"// resources.h - SG24Edit graphics\n\n");

	for( int f = 0; f < 28; ++f )
	{
		cout << "File " << files[f] << " (" << flush;

		cout << ProcessFile( files[f], pafuera ) << " bytes)" << endl;
		//cout << "File " << f << " -> " << files[f] << " (";

		//string fina = "./res/" + files[f];

		//cout << filesize(fina.c_str()) << " bytes)" << endl;
	}

	//pafuera.close();
	fclose(pafuera);

	return 0;
}
