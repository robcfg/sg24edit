////////////////////////////////////////////////////////////
//
// FloydSteinberg.cpp - Implementation of the functions in
//                      the FloydSteinberg namespace.
//
// Created by         : Roberto Carlos Fernandez Gerhardt
//                      05/01/2012
//
////////////////////////////////////////////////////////////

#include "FloydSteinberg.h"
#include <limits.h>  // for INT_MAX
#include <algorithm> // for min() and max()

using namespace std;

int FloydSteinberg::Square( int val )
{
	return (val*val);
}

FloydSteinberg::SFloydSteinberg_Pixel FloydSteinberg::GetPixel( const unsigned char* pSrc, int x, int y, int width, int height )
{
	FloydSteinberg::SFloydSteinberg_Pixel retVal;

	int pos = ((y * width) + x) * 3;
	retVal.R = pSrc[pos++];
	retVal.G = pSrc[pos++];
	retVal.B = pSrc[pos];

	return retVal;
}

void FloydSteinberg::SetPixel( const FloydSteinberg::SFloydSteinberg_Pixel& src, unsigned char* pDst, int x, int y, int width, int height )
{
	int pos = ((y * width) + x) * 3;
	pDst[pos++] = src.R;
	pDst[pos++] = src.G;
	pDst[pos]   = src.B;
}

// Locates the nearest color to the one given from the current palette.
FloydSteinberg::SFloydSteinberg_Pixel FloydSteinberg::FindNearest( const FloydSteinberg::SFloydSteinberg_Pixel& src, const FloydSteinberg::SFloydSteinberg_Palette& palette )
{
	FloydSteinberg::SFloydSteinberg_Pixel retVal;
	int dist = INT_MAX;

	for( unsigned int color = 0; color < palette.colorsNum; ++color )
	{
		int colorDist = Square(src.R - palette.colors[color].R) + Square(src.G - palette.colors[color].G) + Square(src.B - palette.colors[color].B);

		if( colorDist < dist )
		{
			dist = colorDist;
			retVal = palette.colors[color];
		}
	}

	return retVal;
}

// Applies the pixel error to the selected pixel, updating its value.
void FloydSteinberg::ApplyError( unsigned char* pDst, int x, int y, int width, int height, int errorR, int errorG, int errorB )
{
	if( x < 0 || x >= width || y < 0 || y >= height )
		return;

	FloydSteinberg::SFloydSteinberg_Pixel newVal = GetPixel( pDst, x, y, width, height );

	int newR = (int)newVal.R + errorR;
	newR = min(255,newR);
	newR = max(0,newR);
	newVal.R = newR;

	int newG = (int)newVal.G + errorG;
	newG = min(255,newG);
	newG = max(0,newG);
	newVal.G = newG;

	int newB = (int)newVal.B + errorB;
	newB = min(255,newB);
	newB = max(0,newB);
	newVal.B = newB;

	SetPixel( newVal, pDst, x, y, width, height );
}

// Perform the Floyd-Steinberg Error Diffusion dither on a given image with a given palette.
void FloydSteinberg::ProcessImage( unsigned char* pImage, int width, int height, SFloydSteinberg_Palette& palette )
{
	for( int Y = 0; Y < height; ++Y )
	{
		for( int X = 0; X < width; ++X )
		{
			FloydSteinberg::SFloydSteinberg_Pixel oldPixel = GetPixel( pImage, X, Y, width, height );
			FloydSteinberg::SFloydSteinberg_Pixel newPixel = FindNearest( oldPixel, palette );
			SetPixel( newPixel, pImage, X, Y, width, height );
			
			int quantErrorR = (int)oldPixel.R - (int)newPixel.R;
			int quantErrorG = (int)oldPixel.G - (int)newPixel.G;
			int quantErrorB = (int)oldPixel.B - (int)newPixel.B;
 
			ApplyError( pImage, X+1, Y  , width, height, (int)((7.0/16.0)*quantErrorR), (int)((7.0/16.0)*quantErrorG), (int)((7.0/16.0)*quantErrorB) );
			ApplyError( pImage, X-1, Y+1, width, height, (int)((3.0/16.0)*quantErrorR), (int)((3.0/16.0)*quantErrorG), (int)((3.0/16.0)*quantErrorB) );
			ApplyError( pImage, X  , Y+1, width, height, (int)((5.0/16.0)*quantErrorR), (int)((5.0/16.0)*quantErrorG), (int)((5.0/16.0)*quantErrorB) );
			ApplyError( pImage, X+1, Y+1, width, height, (int)((1.0/16.0)*quantErrorR), (int)((1.0/16.0)*quantErrorG), (int)((1.0/16.0)*quantErrorB) );
		}
	}
}
