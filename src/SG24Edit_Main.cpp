#define _CRT_SECURE_NO_WARNINGS

#ifdef _WIN32
#include <windows.h>
#include "SDL.h"
#else
#include <unistd.h>
#include "SDL2/SDL.h"
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))
#endif
#include <stdlib.h>
#include <stdio.h>
#include "FloydSteinberg.h"
#include "tinyfiledialogs.h"

#include "resources.h"
#include "hgversion.h"

// defined
#define ICON_SIZE 32
#define ICON_BLOCK_X 798
#define ICON_BLOCK_Y 70
#define SG24IMAGE_PIXELCOUNT 6144

char APP_CAPTION[1024] = { 0 };

// v0.4 adds Floyd-Steinberg dither of BMP images on loading.

// Enums
enum ECellColor
{
  CC_VOID = 0,
  CC_BLACK,
  CC_GREEN,
  CC_GREEN_BLACK,
  CC_BLACK_GREEN,
  CC_RED,
  CC_RED_BLACK,
  CC_BLACK_RED,
  CC_YELLOW,
  CC_YELLOW_BLACK,
  CC_BLACK_YELLOW,
  CC_BLUE,
  CC_BLUE_BLACK,
  CC_BLACK_BLUE,
  CC_WHITE,
  CC_WHITE_BLACK,
  CC_BLACK_WHITE,
  CC_MAGENTA,
  CC_MAGENTA_BLACK,
  CC_BLACK_MAGENTA,
  CC_ORANGE,
  CC_ORANGE_BLACK,
  CC_BLACK_ORANGE,
  CC_CYAN,
  CC_CYAN_BLACK,
  CC_BLACK_CYAN,
  CC_COUNT
};

// Variables
SDL_Window* mainWindow = 0; 
SDL_Surface* Display = 0;
SDL_Surface* AppIcon = 0;
SDL_Surface* Background = 0;
SDL_Surface* GrayedIcons = 0;
SDL_Surface* temp = 0;
SDL_Surface* Tiles[CC_COUNT];
SDL_Rect src;
SDL_Rect dest;
SDL_Rect IconRects[26];
SDL_Rect Canvas;
SDL_Rect SaveButton;
SDL_Rect SaveAsButton;
SDL_Rect LoadButton;
SDL_Rect ResetButton;
ECellColor curColor = CC_GREEN;
ECellColor SG24Image[SG24IMAGE_PIXELCOUNT];
char curFileName[1024] = {0};

                         // Black, Green, Red, Yellow, Blue, White, Magenta, Orange, Cyan
unsigned char ColorsR[9] = {    0,     0, 255,    255,    0,   255,     255,    255,    0 };
unsigned char ColorsG[9] = {    0,   255,   0,    255,    0,   255,       0,    128,  255 };
unsigned char ColorsB[9] = {    0,     0,   0,      0,  255,   255,     255,      0,  255 };
ECellColor PureColors[9] = { CC_BLACK, CC_GREEN, CC_RED, CC_YELLOW, CC_BLUE, CC_WHITE, CC_MAGENTA, CC_ORANGE, CC_CYAN };
FloydSteinberg::SFloydSteinberg_Palette Palette;

void InitPalette()
{
  Palette.colorsNum = 9;
  Palette.colors = new FloydSteinberg::SFloydSteinberg_Pixel[Palette.colorsNum];

  Palette.colors[0].R = 0;   Palette.colors[0].G = 0;   Palette.colors[0].B = 0;   // Black
  Palette.colors[1].R = 0;   Palette.colors[1].G = 255; Palette.colors[1].B = 0;   // Green
  Palette.colors[2].R = 255; Palette.colors[2].G = 0;   Palette.colors[2].B = 0;   // Red
  Palette.colors[3].R = 255; Palette.colors[3].G = 255; Palette.colors[3].B = 0;   // Yellow
  Palette.colors[4].R = 0;   Palette.colors[4].G = 0;   Palette.colors[4].B = 255; // Blue
  Palette.colors[5].R = 255; Palette.colors[5].G = 255; Palette.colors[5].B = 255; // White
  Palette.colors[6].R = 255; Palette.colors[6].G = 0;   Palette.colors[6].B = 255; // Magenta
  Palette.colors[7].R = 255; Palette.colors[7].G = 127; Palette.colors[7].B = 0;   // Orange
  Palette.colors[8].R = 0;   Palette.colors[8].G = 255; Palette.colors[8].B = 255; // Black
}

void ClearSG24Image()
{
  for( int i = 0; i < SG24IMAGE_PIXELCOUNT; ++i )
  {
    SG24Image[i] = CC_VOID;
  }

  curFileName[0] = 0;

  SDL_BlitSurface(Background, &Canvas, Display, &Canvas);
  SDL_UpdateWindowSurface(mainWindow);

  SDL_SetWindowTitle(mainWindow,APP_CAPTION);
  SDL_SetWindowIcon(mainWindow,AppIcon);
}

void InitRects()
{
  int curX = ICON_BLOCK_X;
  int curY = ICON_BLOCK_Y;
  int iconNum = (int)CC_GREEN;

  for( int y = 0; y < 8; ++y )
  {
    for( int x = 0; x < 3; ++x )
    {
      IconRects[iconNum].x = curX;
      IconRects[iconNum].y = curY;
      IconRects[iconNum].w = ICON_SIZE;
      IconRects[iconNum].h = ICON_SIZE;

      curX += ICON_SIZE + 1;
      ++iconNum;
    }

    curX = ICON_BLOCK_X;
    curY += ICON_SIZE + 1;
  }

  IconRects[(int)CC_BLACK].x = curX;
  IconRects[(int)CC_BLACK].y = curY;
  IconRects[(int)CC_BLACK].w = ICON_SIZE;
  IconRects[(int)CC_BLACK].h = ICON_SIZE;

  Canvas.x = 2;
  Canvas.y = 2;
  Canvas.w = 768;
  Canvas.h = 576;

  SaveButton.x = 786;
  SaveButton.y = 397;
  SaveButton.w = 115;
  SaveButton.h = 28;

  SaveAsButton.x = 786;
  SaveAsButton.y = 434;
  SaveAsButton.w = 115;
  SaveAsButton.h = 28;

  LoadButton.x = 786;
  LoadButton.y = 470;
  LoadButton.w = 115;
  LoadButton.h = 28;

  ResetButton.x = 786;
  ResetButton.y = 505;
  ResetButton.w = 115;
  ResetButton.h = 28;
}

void SelectIcon( ECellColor iconIdx )
{
  // Erase selection
  SDL_BlitSurface(Background, &IconRects[curColor], Display, &IconRects[curColor]);

  // Set new color
  curColor = iconIdx;

  src = IconRects[(int)iconIdx];
  src.x -= ICON_BLOCK_X;
  src.y -= ICON_BLOCK_Y;

  SDL_BlitSurface(GrayedIcons, &src, Display, &IconRects[(int)iconIdx]);
  SDL_UpdateWindowSurface(mainWindow);
}

void SetCell( int x, int y, ECellColor color, bool bFlip = true )
{
  if( color == CC_VOID )
    return;

  int idx = (y*32)+x;
  
  SG24Image[idx] = color;

  src.x = 0;
  src.y = 0;
  src.w = Tiles[(int)color]->w;
  src.h = Tiles[(int)color]->h;

  dest.x = Canvas.x + (x*24);
  dest.y = Canvas.y + (y*3);
  dest.w = Tiles[(int)color]->w;
  dest.h = Tiles[(int)color]->h;

  SDL_BlitSurface(Tiles[(int)color], &src, Display, &dest);
  
  if( bFlip )
	  SDL_UpdateWindowSurface(mainWindow);
}

void SetCell( int idx, ECellColor color, bool bFlip = true )
{
  SG24Image[idx] = color;

  int y = idx / 32;
  int x = idx % 32;

  src.x = 0;
  src.y = 0;
  src.w = Tiles[(int)color]->w;
  src.h = Tiles[(int)color]->h;

  dest.x = Canvas.x + (x*24);
  dest.y = Canvas.y + (y*3);
  dest.w = Tiles[(int)color]->w;
  dest.h = Tiles[(int)color]->h;

  SDL_BlitSurface(Tiles[(int)color], &src, Display, &dest);
  
  if( bFlip )
	  SDL_UpdateWindowSurface(mainWindow);
}

bool IsInRect( int x, int y, const SDL_Rect& rect )
{
  if( x >= rect.x && x < (rect.x+rect.w) && y >=rect.y && y < (rect.y+rect.h) )
    return true;

  return false;
}

bool IsBMP(const char* str)
{
  if( strlen(str) < 3 )
    return false;

  if( (str[0] == 'b' ||  str[0] == 'B') &&
      (str[1] == 'm' ||  str[1] == 'M') &&
      (str[2] == 'p' ||  str[2] == 'P') )
      return true;

  return false;
}

void CellColor2RGBAs( ECellColor color, Uint32& rgba1, Uint32& rgba2 )
{
  if( color == CC_GREEN )
  {
    rgba1 = 0x00FF00FF;
    rgba2 = 0x00FF00FF;
  }
  else if( color == CC_GREEN_BLACK )
  {
    rgba1 = 0x00FF00FF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_GREEN )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0x00FF00FF;
  }
  else if( color == CC_RED )
  {
    rgba1 = 0xFF0000FF;
    rgba2 = 0xFF0000FF;
  }
  else if( color == CC_RED_BLACK )
  {
    rgba1 = 0xFF0000FF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_RED )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0xFF0000FF;
  }
  else if( color == CC_YELLOW )
  {
    rgba1 = 0xFFFF00FF;
    rgba2 = 0xFFFF00FF;
  }
  else if( color == CC_YELLOW_BLACK )
  {
    rgba1 = 0xFFFF00FF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_YELLOW )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0xFFFF00FF;
  }
  else if( color == CC_BLUE )
  {
    rgba1 = 0x0000FFFF;
    rgba2 = 0x0000FFFF;
  }
  else if( color == CC_BLUE_BLACK )
  {
    rgba1 = 0x0000FFFF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_BLUE )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0x0000FFFF;
  }
  else if( color == CC_WHITE )
  {
    rgba1 = 0xFFFFFFFF;
    rgba2 = 0xFFFFFFFF;
  }
  else if( color == CC_WHITE_BLACK )
  {
    rgba1 = 0xFFFFFFFF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_WHITE )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0xFFFFFFFF;
  }
  else if( color == CC_MAGENTA )
  {
    rgba1 = 0xFF00FFFF;
    rgba2 = 0xFF00FFFF;
  }
  else if( color == CC_MAGENTA_BLACK )
  {
    rgba1 = 0xFF00FFFF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_MAGENTA )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0xFF00FFFF;
  }
  else if( color == CC_ORANGE )
  {
    rgba1 = 0xFF8000FF;
    rgba2 = 0xFF8000FF;
  }
  else if( color == CC_ORANGE_BLACK )
  {
    rgba1 = 0xFF8000FF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_ORANGE )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0xFF8000FF;
  }
  else if( color == CC_CYAN )
  {
    rgba1 = 0x00FFFFFF;
    rgba2 = 0x00FFFFFF;
  }
  else if( color == CC_CYAN_BLACK )
  {
    rgba1 = 0x00FFFFFF;
    rgba2 = 0x000000FF;
  }
  else if( color == CC_BLACK_CYAN )
  {
    rgba1 = 0x000000FF;
    rgba2 = 0x00FFFFFF;
  }
  else // Black, or unknown
  {
    rgba1 = 0x000000FF;
    rgba2 = 0x000000FF;
  }
}

bool SaveBMP( const char* fileName )
{
  // Create RGBA surface
  SDL_PixelFormat pixFmt;
  pixFmt.palette       = NULL;
  pixFmt.BitsPerPixel  = 32;
  pixFmt.BytesPerPixel = 4;
  pixFmt.Rmask         = 0xFF000000;
  pixFmt.Rshift        = 24;
  pixFmt.Rloss         = 0;
  pixFmt.Gmask         = 0x00FF0000;
  pixFmt.Gshift        = 16;
  pixFmt.Gloss         = 0;
  pixFmt.Bmask         = 0x0000FF00;
  pixFmt.Bshift        = 8;
  pixFmt.Bloss         = 0;
  pixFmt.Amask         = 0x000000FF;
  pixFmt.Ashift        = 0;
  pixFmt.Aloss         = 0;

  SDL_Surface* convSurf = SDL_CreateRGBSurface( SDL_SWSURFACE, 64, 192, 32, 0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF );
  if( NULL == convSurf )
  {
    printf("Unable to create RGB surface for .BMP conversion (%s)\n", SDL_GetError());
    return false;
  }  
  
  // Map pixels and fill surface (As the surface is a SDL_SWSURFACE, it doesn't need to be locked to access the pixel data
  Uint32* pixels = (Uint32*)convSurf->pixels;
  int idx = 0;
  int cellIdx = 0;
  Uint32 rgba1 = 0, rgba2 = 0;

  for( int y = 0; y < 192; ++y )
  {
    for( int x = 0; x < 64; x+=2 )
    {
      idx = (y*(convSurf->pitch/4)) + x;
      cellIdx = (y*32) + (x/2);

      CellColor2RGBAs(SG24Image[cellIdx],rgba1,rgba2);

      pixels[idx  ] = rgba1;
      pixels[idx+1] = rgba2;
    }
  }

  // Save surface to desired .BMP file
  if( -1 == SDL_SaveBMP(convSurf,fileName) )
  {
    SDL_FreeSurface(convSurf);

    char tmp[2048] = {0};
    sprintf(tmp,"Could not save file %s.\n(%s)",curFileName,SDL_GetError());
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ERROR!", tmp, mainWindow);
    return false;
  }

  // Clean and return
  SDL_FreeSurface(convSurf);

  return true;
}

bool Save()
{
  if( strlen(curFileName) >= 3 && IsBMP(&curFileName[strlen(curFileName)-3]) )
  {
    return SaveBMP( curFileName );
  }

  FILE* outFile = fopen(curFileName,"wb");
  if( NULL == outFile )
  {
    char tmp[2048] = {0};
    sprintf(tmp,"File %s\ncould not be opened for writing.",curFileName);
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ERROR!", tmp, mainWindow);
    curFileName[0] = 0;
    return false;
  }

  unsigned char uVal = 0;
  for( int i = 0; i < SG24IMAGE_PIXELCOUNT; ++i )
  {
    if( SG24Image[i] == CC_BLACK )
      uVal = 128;
    else if( SG24Image[i] == CC_GREEN )
      uVal = 143;
    else if( SG24Image[i] == CC_GREEN_BLACK )
      uVal = 138;
    else if( SG24Image[i] == CC_BLACK_GREEN )
      uVal = 133;
    else if( SG24Image[i] == CC_RED )
      uVal = 191;
    else if( SG24Image[i] == CC_RED_BLACK )
      uVal = 186;
    else if( SG24Image[i] == CC_BLACK_RED )
      uVal = 181;
    else if( SG24Image[i] == CC_YELLOW )
      uVal = 159;
    else if( SG24Image[i] == CC_YELLOW_BLACK )
      uVal = 154;
    else if( SG24Image[i] == CC_BLACK_YELLOW )
      uVal = 149;
    else if( SG24Image[i] == CC_BLUE )
      uVal = 175;
    else if( SG24Image[i] == CC_BLUE_BLACK )
      uVal = 170;
    else if( SG24Image[i] == CC_BLACK_BLUE )
      uVal = 165;
    else if( SG24Image[i] == CC_WHITE )
      uVal = 207;
    else if( SG24Image[i] == CC_WHITE_BLACK )
      uVal = 202;
    else if( SG24Image[i] == CC_BLACK_WHITE )
      uVal = 197;
    else if( SG24Image[i] == CC_MAGENTA )
      uVal = 239;
    else if( SG24Image[i] == CC_MAGENTA_BLACK )
      uVal = 234;
    else if( SG24Image[i] == CC_BLACK_MAGENTA )
      uVal = 229;
    else if( SG24Image[i] == CC_ORANGE )
      uVal = 255;
    else if( SG24Image[i] == CC_ORANGE_BLACK )
      uVal = 250;
    else if( SG24Image[i] == CC_BLACK_ORANGE )
      uVal = 245;
    else if( SG24Image[i] == CC_CYAN )
      uVal = 223;
    else if( SG24Image[i] == CC_CYAN_BLACK )
      uVal = 218;
    else if( SG24Image[i] == CC_BLACK_CYAN )
      uVal = 213;
    else
      uVal = 32; // Seen as black but used to load it as void

    fwrite(&uVal,1,1,outFile);
  }
  fclose(outFile);
  
  return true;
}

void SaveAs()
{
  char const * filterPatterns[3] = { "*.S24" , "*.bmp" };
  
  char const * lTheOpenFileName;
  lTheOpenFileName = tinyfd_saveFileDialog("Select file to save", NULL, 2, filterPatterns, NULL);

  if( lTheOpenFileName )
  {
    strcpy(curFileName,lTheOpenFileName);

    if( IsBMP(&lTheOpenFileName[strlen(lTheOpenFileName)-3]) )
    {
      if( SaveBMP( lTheOpenFileName ) )
      {
        char tmp[256] = {0};
        sprintf(tmp,"%s - %s", APP_CAPTION, lTheOpenFileName);
        SDL_SetWindowTitle(mainWindow,tmp);
      }
    }
    else
    {
      if( Save() )
      {
        char tmp[256] = {0};
        sprintf(tmp,"%s - %s", APP_CAPTION, lTheOpenFileName);
        SDL_SetWindowTitle(mainWindow,tmp);
      }
    }
  }
}

ECellColor GetNearest( Uint32 color, const SDL_PixelFormat* fmt )
{
  ECellColor retval = CC_VOID;

  FloydSteinberg::SFloydSteinberg_Pixel src;
  SDL_GetRGB( color, fmt, &src.R, &src.G, &src.B );
  FloydSteinberg::SFloydSteinberg_Pixel nearest = FloydSteinberg::FindNearest(src,Palette);

  int rgb = 0;

  while( rgb < 9 && retval == CC_VOID )
  {
    if( nearest == Palette.colors[rgb] )
    {
      retval = PureColors[rgb];
    }

    ++rgb;
  }

  return retval;
}

SDL_Surface* LoadBMP_Mem( const unsigned char* data, size_t datasize )
{
  SDL_RWops* rw = SDL_RWFromConstMem(data, datasize);
  
  SDL_Surface* pSurf = SDL_LoadBMP_RW(rw, 1);
  if( !pSurf )
    printf("%s",SDL_GetError());
  return pSurf;
}

void LoadBMP( const char* fileName )
{
  // Load Image
  temp = SDL_LoadBMP(fileName);
  if (temp == NULL) {
	  printf("Unable to load %s: %s\n", fileName, SDL_GetError());
    return;
  }

  if( temp->w < 2 )
  {
    char tmp[2048] = {0};
    sprintf(tmp,"Image %s\nmust be at least 2 pixels wide.",fileName);
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ERROR!", tmp, mainWindow);
    ClearSG24Image();
    curFileName[0] = 0;
    return;
  }

  // Convert to RGBA
  SDL_PixelFormat pixFmt;
  pixFmt.palette       = NULL;
  pixFmt.BitsPerPixel  = 32;
  pixFmt.BytesPerPixel = 4;
  pixFmt.Rmask         = 0xFF000000;
  pixFmt.Rshift        = 24;
  pixFmt.Rloss         = 0;
  pixFmt.Gmask         = 0x00FF0000;
  pixFmt.Gshift        = 16;
  pixFmt.Gloss         = 0;
  pixFmt.Bmask         = 0x0000FF00;
  pixFmt.Bshift        = 8;
  pixFmt.Bloss         = 0;
  pixFmt.Amask         = 0x000000FF;
  pixFmt.Ashift        = 0;
  pixFmt.Aloss         = 0;

  SDL_Surface* convSurf = SDL_ConvertSurface(temp, &pixFmt, SDL_SWSURFACE);
  if( NULL == convSurf )
  {
    printf("Unable to convert %s: %s\n", fileName, SDL_GetError());
    return;
  }

  // Lock surface if necessary
  bool mustLock = (0 != SDL_MUSTLOCK(temp));
  int iLocked = 0;

  if( mustLock )
  {
    iLocked = SDL_LockSurface( temp );
    if( iLocked == 0 || iLocked == -1 )
    {
      printf("Unable to lock surface of %s: %s\n", fileName, SDL_GetError());
      SDL_FreeSurface(convSurf);
      SDL_FreeSurface(temp);
      return;
    }
  }

  // Set final image
  ClearSG24Image();

  // Dither image and convert to Dragon palette
  int maxX = min(64 ,temp->w);
  int maxY = min(192,temp->h);

  const SDL_MessageBoxButtonData buttons[] = 
  {
	  { /* .flags, .buttonid, .text */        0, 0, "no" },
	  { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "yes" },
  };
  const SDL_MessageBoxColorScheme colorScheme = 
  {
	  { /* .colors (.r, .g, .b) */
		/* [SDL_MESSAGEBOX_COLOR_BACKGROUND] */
		  { 39,   40,   32 },
		  /* [SDL_MESSAGEBOX_COLOR_TEXT] */
		  { 248, 248,   242 },
		  /* [SDL_MESSAGEBOX_COLOR_BUTTON_BORDER] */
		  { 108, 109,   105 },
		  /* [SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND] */
		  { 59,   50, 42 },
		  /* [SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED] */
		  { 113, 207, 45 }
	  }
  };
  const SDL_MessageBoxData messageboxdata = 
  {
	  SDL_MESSAGEBOX_INFORMATION, /* .flags */
	  mainWindow, /* .window */
	  "SG24Edit needs to know...", /* .title */
	  "Do you want to dither image?", /* .message */
	  SDL_arraysize(buttons), /* .numbuttons */
	  buttons, /* .buttons */
	  &colorScheme /* .colorScheme */
  };

  int buttonid = 0;

  if( SDL_ShowMessageBox(&messageboxdata,&buttonid) >= 0 && buttonid == 1 )
  {
    FloydSteinberg::ProcessImage((unsigned char*)convSurf->pixels,maxX,maxY,Palette);
  }

  // Process image
  Uint32* pixels = (Uint32*)convSurf->pixels;
  int coordX = 0;
  int idx = 0;

  if( maxX % 2 != 0 )
    maxX -= 1;

  for( int y = 0; y < maxY; ++y )
  {
    for( int x = 0; x < maxX; x+=2 )
    {
      idx = ((y * (convSurf->pitch/4)) + x);
      coordX = x/2;

      Uint32 pix1 = pixels[idx];
      Uint32 pix2 = pixels[idx+1];

      ECellColor col1 = GetNearest(pix1,&pixFmt);
      ECellColor col2 = GetNearest(pix2,&pixFmt);

      if( col1 == CC_BLACK )
      {
        if( col2 == CC_BLACK ) SetCell( coordX, y, CC_BLACK, false );
        else if( col2 == CC_GREEN   ) SetCell( coordX, y, CC_BLACK_GREEN  , false );
        else if( col2 == CC_RED     ) SetCell( coordX, y, CC_BLACK_RED    , false );
        else if( col2 == CC_YELLOW  ) SetCell( coordX, y, CC_BLACK_YELLOW , false );
        else if( col2 == CC_BLUE    ) SetCell( coordX, y, CC_BLACK_BLUE   , false );
        else if( col2 == CC_WHITE   ) SetCell( coordX, y, CC_BLACK_WHITE  , false );
        else if( col2 == CC_MAGENTA ) SetCell( coordX, y, CC_BLACK_MAGENTA, false );
        else if( col2 == CC_ORANGE  ) SetCell( coordX, y, CC_BLACK_ORANGE , false );
        else if( col2 == CC_CYAN    ) SetCell( coordX, y, CC_BLACK_CYAN   , false );
      }
      else if( col2 == CC_BLACK )
      {
        if( col1 == CC_GREEN   ) SetCell( coordX, y, CC_GREEN_BLACK  , false );
        else if( col1 == CC_RED     ) SetCell( coordX, y, CC_RED_BLACK    , false );
        else if( col1 == CC_YELLOW  ) SetCell( coordX, y, CC_YELLOW_BLACK , false );
        else if( col1 == CC_BLUE    ) SetCell( coordX, y, CC_BLUE_BLACK   , false );
        else if( col1 == CC_WHITE   ) SetCell( coordX, y, CC_WHITE_BLACK  , false );
        else if( col1 == CC_MAGENTA ) SetCell( coordX, y, CC_MAGENTA_BLACK, false );
        else if( col1 == CC_ORANGE  ) SetCell( coordX, y, CC_ORANGE_BLACK , false );
        else if( col1 == CC_CYAN    ) SetCell( coordX, y, CC_CYAN_BLACK   , false );
      }
      else
      {
        Uint8 r1 = 0;
        Uint8 g1 = 0;
        Uint8 b1 = 0;
        Uint8 r2 = 0;
        Uint8 g2 = 0;
        Uint8 b2 = 0;

        SDL_GetRGB( pix1, &pixFmt, &r1, &g1, &b1 );
        SDL_GetRGB( pix2, &pixFmt, &r2, &g2, &b2 );

        Uint32 newRGB = SDL_MapRGB( &pixFmt, (r1+r2)/2, (g1+g2)/2, (b1+b2)/2 );
        ECellColor newColor = GetNearest( newRGB, &pixFmt );
        SetCell( coordX, y, newColor, false );
      }
    }
  }

  // Unlock and clean
  if( mustLock )
  {
    SDL_UnlockSurface(temp);
  }

  SDL_FreeSurface(convSurf);
  SDL_FreeSurface(temp);
  SDL_UpdateWindowSurface(mainWindow);
}

void Load()
{
	char const * filterPatterns[3] = { "*.S24" , "*.bmp" };
	
	char const * lTheOpenFileName;
	lTheOpenFileName = tinyfd_openFileDialog("Select file to open", NULL, 2, filterPatterns, NULL, 0);

	if( lTheOpenFileName )
	{
		if( IsBMP( &lTheOpenFileName[strlen(lTheOpenFileName)-3]) )
		{
			LoadBMP(lTheOpenFileName);

			if(lTheOpenFileName[0] == 0)
			{
				SDL_SetWindowTitle(mainWindow,APP_CAPTION);
				return;
			}

			strcpy(curFileName, lTheOpenFileName);
			strcpy(&curFileName[strlen(lTheOpenFileName) - 3], "S24");
			char tmp[256] = {0};
			sprintf(tmp,"%s - %s", APP_CAPTION, curFileName);
			SDL_SetWindowTitle(mainWindow,tmp);

			return;
		}

		FILE* inFile = fopen(lTheOpenFileName,"rb");
		if( NULL == inFile )
		{
		  char tmp[2048] = {0};
		  sprintf(tmp,"File %s\ncould not be opened for error.", lTheOpenFileName);
		  SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ERROR!", tmp, mainWindow);
		  curFileName[0] = 0;
		  return;
		}

	    ClearSG24Image();

		unsigned char uVal = 0;
		for( int i = 0; i < SG24IMAGE_PIXELCOUNT; ++i )
		{
			fread(&uVal,1,1,inFile);

			switch( uVal )
			{
			case 128:SetCell(i,CC_BLACK,false);break;
			case 143:SetCell(i,CC_GREEN,false);break;
			case 138:SetCell(i,CC_GREEN_BLACK,false);break;
			case 133:SetCell(i,CC_BLACK_GREEN,false);break;
			case 191:SetCell(i,CC_RED,false);break;
			case 186:SetCell(i,CC_RED_BLACK,false);break;
			case 181:SetCell(i,CC_BLACK_RED,false);break;
			case 159:SetCell(i,CC_YELLOW,false);break;
			case 154:SetCell(i,CC_YELLOW_BLACK,false);break;
			case 149:SetCell(i,CC_BLACK_YELLOW,false);break;
			case 175:SetCell(i,CC_BLUE,false);break;
			case 170:SetCell(i,CC_BLUE_BLACK,false);break;
			case 165:SetCell(i,CC_BLACK_BLUE,false);break;
			case 207:SetCell(i,CC_WHITE,false);break;
			case 202:SetCell(i,CC_WHITE_BLACK,false);break;
			case 197:SetCell(i,CC_BLACK_WHITE,false);break;
			case 239:SetCell(i,CC_MAGENTA,false);break;
			case 234:SetCell(i,CC_MAGENTA_BLACK,false);break;
			case 229:SetCell(i,CC_BLACK_MAGENTA,false);break;
			case 255:SetCell(i,CC_ORANGE,false);break;
			case 250:SetCell(i,CC_ORANGE_BLACK,false);break;
			case 245:SetCell(i,CC_BLACK_ORANGE,false);break;
			case 223:SetCell(i,CC_CYAN,false);break;
			case 218:SetCell(i,CC_CYAN_BLACK,false);break;
			case 213:SetCell(i,CC_BLACK_CYAN,false);break;
			default:break;
			}
		}

		fclose(inFile);

		strcpy(curFileName, lTheOpenFileName);
		char tmp[256] = {0};
		//sprintf(tmp,"%s - %s", APP_CAPTION, &curFileName[ofn.nFileOffset]);
		sprintf(tmp, "%s - %s", APP_CAPTION, lTheOpenFileName);
		SDL_SetWindowTitle(mainWindow,tmp);

		SDL_UpdateWindowSurface(mainWindow);
	}
}

int main( int argc, char* argv[] )
{
  if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
          fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
          return EXIT_FAILURE;
      }
  atexit(SDL_Quit);  
  
  // Body of the program goes here.
  // Load App icon
  AppIcon = LoadBMP_Mem(SG24Icon,SG24Icon_size);

  // Compose window title
  sprintf(APP_CAPTION, "SemiGraphics 24 Image Editor by Robcfg (%s)", hgVersion);

  SDL_SetWindowTitle(mainWindow, APP_CAPTION);
  SDL_SetWindowIcon(mainWindow,AppIcon);

  InitRects();
  InitPalette();

  // Init Window
  mainWindow = SDL_CreateWindow("AbadiaSDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 922, 580, SDL_WINDOW_OPENGL);
  if (!mainWindow)
  {
	  return EXIT_FAILURE;
  }
  Display = SDL_GetWindowSurface(mainWindow);

  // Load background img
  temp = LoadBMP_Mem(SG24Scrn,SG24Scrn_size);
  Background = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);

  // Load grayed icons
  temp = LoadBMP_Mem(SG24IcnG,SG24IcnG_size);
  GrayedIcons = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);

  // Load Tiles
  int tile = 1;
  temp = LoadBMP_Mem(Tile1,Tile1_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile2,Tile2_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile3,Tile3_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile4,Tile4_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile5,Tile5_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile6,Tile6_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile7,Tile7_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile8,Tile8_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile9,Tile9_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile10,Tile10_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile11,Tile11_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile12,Tile12_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile13,Tile13_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile14,Tile14_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile15,Tile15_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile16,Tile16_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile17,Tile17_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile18,Tile18_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile19,Tile19_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile20,Tile20_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile21,Tile21_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile22,Tile22_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile23,Tile23_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile24,Tile24_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);
  temp = LoadBMP_Mem(Tile25,Tile25_size);
  Tiles[tile++] = SDL_ConvertSurface(temp, Display->format, 0);
  SDL_FreeSurface(temp);

  // Blit background
  src.x = 0;
  src.y = 0;
  src.w = Background->w;
  src.h = Background->h;
   
  dest.x = 0;
  dest.y = 0;
  dest.w = Background->w/2;
  dest.h = Background->h;

  SDL_BlitSurface(Background, &src, Display, &dest);

  // Clear Canvas
  ClearSG24Image();

  // Select 1st Icon
  SelectIcon(curColor);

  // Show screen
  SDL_UpdateWindowSurface(mainWindow);

  while(1)
  {
    // Handle events for this time step
    SDL_Event e;
    if(SDL_WaitEvent(&e))
    {
      if(e.type == SDL_QUIT)
      {
        return EXIT_SUCCESS;
      }
      else if(e.type == SDL_MOUSEBUTTONUP && e.button.button == 1)
      {
        // Check for color change
        for( int icon = CC_BLACK; icon < CC_COUNT; ++icon )
        {
          if( IsInRect(e.motion.x, e.motion.y, IconRects[icon]) )
          {
            SelectIcon( (ECellColor)icon );
            break;
          }
        }

        // Reset Button
        if( IsInRect(e.motion.x, e.motion.y, ResetButton) )
        {
			const SDL_MessageBoxButtonData buttons[] =
			{
				{ /* .flags, .buttonid, .text */        0, 0, "no" },
				{ SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "yes" },
			};
      const SDL_MessageBoxColorScheme colorScheme = 
      {
        { /* .colors (.r, .g, .b) */
        /* [SDL_MESSAGEBOX_COLOR_BACKGROUND] */
          { 39,   40,   32 },
          /* [SDL_MESSAGEBOX_COLOR_TEXT] */
          { 248, 248,   242 },
          /* [SDL_MESSAGEBOX_COLOR_BUTTON_BORDER] */
          { 108, 109,   105 },
          /* [SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND] */
          { 59,   50, 42 },
          /* [SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED] */
          { 113, 207, 45 }
        }
      };
			const SDL_MessageBoxData messageboxdata =
			{
				SDL_MESSAGEBOX_WARNING, /* .flags */
				mainWindow, /* .window */
				"WARNING!", /* .title */
				"You will lose all unsaved changes!\nAre you sure to RESET the image?", /* .message */
				SDL_arraysize(buttons), /* .numbuttons */
				buttons, /* .buttons */
				&colorScheme /* .colorScheme */
			};

			int result = 0;

			if (SDL_ShowMessageBox(&messageboxdata, &result) >= 0 && result == 1)
	            ClearSG24Image();
        }
        else if( IsInRect(e.motion.x, e.motion.y, SaveButton) )
        {
          if( strlen(curFileName) == 0 )
            SaveAs();
          else
            Save();
        }
        else if( IsInRect(e.motion.x, e.motion.y, SaveAsButton) )
        {
          SaveAs();
        }
        else if( IsInRect(e.motion.x, e.motion.y, LoadButton) )
        {
            Load();
        }
      }
      else if((e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEMOTION) && e.button.button == 1 )
      {
        // Paint it Black (or green, or red-black, or yellow, maybe blue dabudee dabuda...)
        if( IsInRect(e.motion.x, e.motion.y, Canvas) )
        {
          int cellX = (e.motion.x - Canvas.x) / 24;
          int cellY = (e.motion.y - Canvas.y) / 3;

          SetCell( cellX, cellY, curColor );
        }
      }
    }
  }

  return EXIT_SUCCESS;
}
