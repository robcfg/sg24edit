//////////////////////////////////////////////////////////////////////////////////////
//
// FloydSteinberg.h - Header file for FloydSteinberg namespace,
//                    which contains functions for performing a
//                    Floyd-Steinberg color reduction and dither of a 24bpp RGB image.
//
// Created by       : Roberto Carlos Fernandez Gerhardt
//                    05/01/2012
//
// References       : http://en.wikipedia.org/wiki/Floyd%E2%80%93Steinberg_dithering
//                    http://en.wikipedia.org/wiki/Color_quantization
//                    http://en.wikipedia.org/wiki/Quantization_(image_processing)
//
//////////////////////////////////////////////////////////////////////////////////////

#ifndef _CFLOYDSTEINBERG_
#define _CFLOYDSTEINBERG_

namespace FloydSteinberg
{
	struct SFloydSteinberg_Pixel
	{
		unsigned char R,G,B;

    bool operator == (const SFloydSteinberg_Pixel& other)
    {
      if( R == other.R && G == other.G && B == other.B )
      {
        return true;
      }

      return false;
    }
	};

	struct SFloydSteinberg_Palette
	{
		unsigned int           colorsNum;
		SFloydSteinberg_Pixel* colors;

		SFloydSteinberg_Palette()  { colors = 0; }
		~SFloydSteinberg_Palette() { if(0 != colors) delete colors; }
	};

	int Square                       ( int val );
	SFloydSteinberg_Pixel GetPixel   ( const unsigned char* pSrc, int x, int y, int width, int height );
	void SetPixel                    ( const SFloydSteinberg_Pixel& src, unsigned char* pDst, int x, int y, int width, int height );
	SFloydSteinberg_Pixel FindNearest( const SFloydSteinberg_Pixel& src, const SFloydSteinberg_Palette& palette );
	void ApplyError                  ( unsigned char* pDst, int x, int y, int width, int height, int errorR, int errorG, int errorB );
	void ProcessImage                ( unsigned char* pImage, int width, int height, SFloydSteinberg_Palette& palette );
}

#endif